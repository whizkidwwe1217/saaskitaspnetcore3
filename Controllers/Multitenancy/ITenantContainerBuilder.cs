using System.Threading.Tasks;
using SaasKit.Multitenancy;
using StructureMap;

namespace saaskitthree.Multitenancy
{
    public interface ITenantContainerBuilder<TTenant>
        where TTenant : class
    {
        //Task<IContainer> BuildAsync(TTenant tenant);
        Task<IContainer> BuildAsync(TenantContext<TTenant> tenant);
    }
}