using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SaasKit.Multitenancy;
using StructureMap;

namespace saaskitthree.Multitenancy
{
    public static class MultitenancyExtensions
    {
        public static IApplicationBuilder UseTenantContainers<TTenant>(this IApplicationBuilder app) where TTenant : class
        {
            return app.UseMiddleware<MultitenantContainerMiddleware<TTenant>>();
        }

        public static IContainer GetTenantContainer<TTenant>(this TenantContext<TTenant> tenantContext) where TTenant : class
        {
            object tenantContainer;

            if (tenantContext.Properties.TryGetValue(CURRENT_TENANT_CONTAINER_KEY, out tenantContainer))
            {
                return tenantContainer as IContainer;
            }
            return null;
        }

        public static void SetTenantContainer<TTenant>(this TenantContext<TTenant> tenantContext, IContainer container) where TTenant : class
        {
            if (tenantContext.Properties.ContainsKey(CURRENT_TENANT_CONTAINER_KEY))
            {
                tenantContext.Properties[CURRENT_TENANT_CONTAINER_KEY] = container;
            }
            else
            {
                tenantContext.Properties.Add(CURRENT_TENANT_CONTAINER_KEY, container);
            }
        }

        public const string CURRENT_TENANT_CONTEXT_KEY = "Blackfish.Multitenancy.CurrentTenantContext";
        public const string CURRENT_TENANT_CONTAINER_KEY = "Blackfish.Multitenancy.CurrentTenantContainer";

        public static TenantContext<TTenant> GetTenantContext<TTenant>(this HttpContext context) where TTenant : class
        {
            object tenantContext;
            if (context.Items.TryGetValue(CURRENT_TENANT_CONTEXT_KEY, out tenantContext))
            {
                return tenantContext as TenantContext<TTenant>;
            }

            return null;
        }

        public static void SetCurrentTenantContext<TTenant>(this HttpContext context, TenantContext<TTenant> tenantContext) where TTenant : class
        {
            if (context.Items.ContainsKey(CURRENT_TENANT_CONTEXT_KEY))
            {
                context.Items[CURRENT_TENANT_CONTEXT_KEY] = tenantContext;
            }
            else
            {
                context.Items.Add(CURRENT_TENANT_CONTEXT_KEY, tenantContext);
            }
        }
    }
}
