using System;
using System.Threading.Tasks;
using SaasKit.Multitenancy;
using StructureMap;

namespace saaskitthree.Multitenancy
{
    public class TenantContainerBuilder<TTenant> : ITenantContainerBuilder<TTenant>
        where TTenant : class
    {
        public TenantContainerBuilder(IContainer container, Action<TTenant, ConfigurationExpression> configure)
        {
            Container = container;
            Configure = configure;
        }

        public IContainer Container { get; }
        public Action<TTenant, ConfigurationExpression> Configure { get; }

        // public virtual async Task<IContainer> BuildAsync(TTenant tenant)
        // {
        //     var tenantContainer = Container.CreateChildContainer();
        //     tenantContainer.Configure(config => Configure(tenant, config));
        //     return await Task.FromResult(tenantContainer);
        // }

        public virtual Task<IContainer> BuildAsync(TenantContext<TTenant> tenant)
        {
            var tenantContainer = Container.CreateChildContainer();
            tenantContainer.Configure(config =>
            {
                config.For<TenantContext<TTenant>>().Use(tenant);
                Configure(tenant.Tenant, config);
            });

            return Task.FromResult(tenantContainer);
        }
    }
}