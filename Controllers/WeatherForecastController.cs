﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace saaskitthree.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly AppTenant tenant;
        private readonly IRepository repository;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, AppTenant tenant, IRepository repository)
        {
            _logger = logger;
            this.tenant = tenant;
            this.repository = repository;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var output = new {
                Tenant = tenant?.Name,
                Database = repository?.Engine
            };
            
            return Ok(output);
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)],
            })
            .ToArray();
        }
    }
}
