namespace saaskitthree
{
    public class MsSqlRepository : IRepository
    {
        public string Engine => "Microsoft SQL Server";
    }
}